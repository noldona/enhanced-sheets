let Enhancement = (superclass) => class extends superclass {
	/** @override */
	activateEditor(name, options={}, initialContent="") {
		super.activateEditor(name, options, initialContent);
		const editor = this.editors[name];
		const div = $(editor.options.target).siblings('.editor-placeholder');
		if (div) {
			$(div).hide();
		}
	}

	/** @override */
	async saveEditor(name, {remove=true}={}) {
		const editor = this.editors[name];
		const content = editor.mce.getContent();
		if (!content) {
			const div = $(editor.options.target).siblings('.editor-placeholder');
			if (div) {
				$(div).show();
			}
		}

		super.saveEditor(name, {remove: remove});
	}
};

export class EnhancedItemSheet extends Enhancement(ItemSheet) {}
export class EnhancedActorSheet extends Enhancement(ActorSheet) {}

Hooks.once('init', async function() {
	Handlebars.unregisterHelper('editor');
	Handlebars.registerHelper('editor', (options) => {
		let results = HandlebarsHelpers.editor(options);
		var doc = document.createElement('div');
		doc.innerHTML = results;
		if (!doc.getElementsByClassName('editor-content')[0].innerHTML) {
			let parent = doc.getElementsByClassName('editor-content')[0].parentElement;
			const owner = Boolean(options.hash['owner']);
			const placeholder = TextEditor.enrichHTML(options.hash['placeholder'] || "", {secrets: owner, entities: true});
			parent.prepend(document.createElement('div'));
			let placeholderDiv = parent.firstElementChild;
			placeholderDiv.innerHTML = `${placeholder}`;
			placeholderDiv.className = "editor-placeholder";
		}
		return new Handlebars.SafeString(doc.innerHTML);
	});
});

Hooks.once('setup', async function() {
	CONFIG.TinyMCE.placeholder = game.i18n.localize('EnhancedSheets.Placeholder');
	CONFIG.TinyMCE.content_style = ".mce-content-body[data-mce-placeholder]:not(.mce-visual-blocks)::before { top: 0.55rem; }";
});
