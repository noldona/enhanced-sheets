const gulp = require('gulp');
const sass = require('gulp-sass');

/**
 * Compile SASS
 */
sass.compiler = require('node-sass');
function compileSASS() {
	return gulp.src('./styles/sass/enhancements.scss')
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
		.pipe(gulp.dest('./styles'));
}
const css = gulp.series(compileSASS);

/**
 * Watch Updates
 */
function watchUpdates() {
	gulp.watch('./styles/sass/**/*.scss', css);
}

/**
 * Export Tasks
 */
exports.default = gulp.series(
	gulp.parallel(css),
	watchUpdates
);
exports.css = css;
