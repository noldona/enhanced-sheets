# Ehanced Sheets for Foundry VTT
This project adds some enhancements to the base ActorSheet and ItemSheet classes in the Foundry VTT environment. This module is intended to be use as a library for system development and does not any use accessible functions on it's own.

# Enhancements
* Added the ability to have placeholder text for the editors

# Usage
Use the `EnhancedActorSheet` or the `EnhancedItemSheet` in place of the regular `ActorSheet` or `ItemSheet` classes for your sheets by importing the class and inherit from that class.

```javascript
import { EnhancedItemSheet } from "/modules/enhanced-sheets/modules/enhancements.js";

export class SystemItemSheet extends EnhancedItemSheet {
	// ...
}
```

Add the placeholder setting to the editor to make the placeholder appear when the editor is closed and empty.
```html
<div class="tab description-tab" data-group="primary" data-tab="description">
	{{editor content=data.description target="data.description" placeholder=(localize 'EnhancedSheets.Placeholder') button=true owner=owner editable=editable}}
</div>
```


# What is Foundry VTT?
Foundry VTT is a virtual tabletop application for playing RPG games over the internet. More information can be found at https://foundryvtt.com/
